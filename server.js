var express = require('express');
const uuidV1 = require('uuid/v1');
var id = uuidV1();
var app = express();

app.get("/",function(req, res, next){
	res.send("timestamp: " + new Date() + " id:" + id);
});

console.log("starting app on port " + (process.env.PORT || 8000) + " id:" + id);
app.listen(process.env.PORT || 8000);